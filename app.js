import express from 'express';
import userRoutes from './src/routes/index.js';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import 'dotenv/config.js';
import { API_VERSION } from './src/utils/constant.js';
const app = express();

app.use(bodyParser.json());
app.use(API_VERSION, userRoutes);

mongoose.connect(process.env.MONGO_URL).then(() => {
    app.listen(process.env.PORT_NUMBER);
}).catch(() => {
    console.log('Connection failed!');
});
