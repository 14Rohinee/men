import jwt from 'jsonwebtoken';

const auth = (req, res, next) => {
    const token = req.headers.authorization;
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    } catch (error) {
        return next(error);
    }

    if (!decodedToken) {
        return next(err);
    }

    req.userData = {userId: decodedToken.userId, email: decodedToken.email};

    next();
};

export default auth;