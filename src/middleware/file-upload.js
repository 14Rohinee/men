import multer  from 'multer';

const upload = multer({
    limits: {
        fileSize: 1000000
    },
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/images/users');
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        }
    })
})

export default upload;