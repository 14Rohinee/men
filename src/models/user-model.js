import userSchema from "../schema/users.js";
import mongoose from "mongoose";

const usersModel = mongoose.model('User', userSchema);

export default usersModel;