import {Schema} from "mongoose";

const userSchema = new Schema({
    name: {
        type: String,
        // required: true
    },
    age: {
        type: Number,
        // required: true
    },
    dob: {
        type: String,
        // required: true
    },
    image: {
        type: String,
    },
    email: {
        type: String,
        // required: true
    },
    password: {
        type: String,
    }
});

export default userSchema;