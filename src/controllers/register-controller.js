import bcrypt from 'bcrypt';
import User from '../models/user-model.js';
import jwt from 'jsonwebtoken';

const register = async(req, res, next) => {
    const {name, email, password} = req.body;
    let hashedPassword;
    let exsistingUser;

    try {
        hashedPassword = await bcrypt.hash(password, 12);
    }
    catch (error) {
        return next(err);
    }

    if (exsistingUser) {
        return next(error);
    }
    else{
        const user = new User({
            name,
            email,
            password: hashedPassword
        });

        try {
            await user.save();
        }
        catch (error) {
            return next(err);
        }

        let token;

        try {
            token = jwt.sign(
                { userId: user.id, email: user.email },
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: '1h'}
            )
        } catch (error) {
            return next(error);
        }

        res.status(201).json({userId: user.id, email: user.email, token: token});
    }

}

export default register;