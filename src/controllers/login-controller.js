import User from '../models/user-model.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

const login = async(req, res, next) => {
    const {email, password} = req.body;
    let existingUser;
    let validPassword;
    let token;
    
    try {
        existingUser = await User.findOne({email: email});
    } catch (error) {
        return next(err);
    }

    try {
        validPassword = await bcrypt.compare(password, existingUser.password);
    }
    catch (error) {
        return next(error);
    }

    try {
        token = jwt.sign(
            { userId: existingUser.id, email: existingUser.email },
            process.env.ACCESS_TOKEN_SECRET,
            { expiresIn: '1m'}
        )
    } catch (error) {
        return next(error);
    }

    if (existingUser && validPassword) {;
        res.json({userId: existingUser.id, email: existingUser.email, token: token})
    } else {
        return next(err);
    }

}

export default login;