import User from '../models/user-model.js';
import { validationResult } from 'express-validator';

const index = (req, res, next) => {
    User.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    });
}

const store = (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    const user = new User({
        name:req.body.name,
        age:req.body.age,
        dob:req.body.dob
    })
    user.save().then((result) => {
        res.send(result);
    }   ).catch((err) => {
        console.log(err);
    });
}

const update = (req, res, next) => {
    // const errors = validationResult(req);

    // if (!errors.isEmpty()) {
    //     return res.status(422).json({ errors: errors.array() });
    // }

    const {name, age, dob} = req.body;
    const id = req.params.id;

    User.findById(id).then((user) => {
        user.name = name;
        user.age = age;
        user.dob = dob;

        return user.save();
    }).then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    });
}

const destroy = (req, res, next) => {
    const id = req.params.id;

    User.findByIdAndRemove(id).then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    });
}


const fileUpload = (req, res) => {
    const user = new User({
        image: req.file.filename
    })
    user.save().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    });
}


export { index, store, update, destroy, fileUpload};