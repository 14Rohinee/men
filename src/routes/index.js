import express from 'express';
import userRoute from './user-routes.js';
import login from '../controllers/login-controller.js';
import register from '../controllers/register-controller.js';
import auth from '../middleware/check-auth.js';

const router = express.Router();

router.post('/login', login);
router.post('/register', register);

router.use(auth);

router.use('/users', userRoute);

export default router;
