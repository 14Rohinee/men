import { Router } from 'express';
import { index, store, update, destroy, fileUpload } from '../controllers/user-controller.js';
import { check } from 'express-validator';
import  upload from '../middleware/file-upload.js';

const router = Router();

router.get('/', index);
router.post('/create', store);
router.put('/update/:id', update);
router.delete('/delete/:id', destroy);
router.post('/file-upload', upload.single('image'), fileUpload);

export default router;